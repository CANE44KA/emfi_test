<?php

namespace App\Controller;

use App\Form\UserForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class FormController extends AbstractController
{
    #[Route('/form', name: 'form_page', methods: 'GET')]
    public function userForm(): Response
    {
        $form = $this->createForm(UserForm::class);

        return $this->render('index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}